FROM iron/go

ENV PORT 3000

EXPOSE 3000

WORKDIR /app

ADD ./app /app/

RUN chmod a+x ./app

ENTRYPOINT [ "./app" ]
