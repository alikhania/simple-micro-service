.PHONY: build serve clean pack deploy ship

export COMMIT_TAG=$(shell git rev-list HEAD --max-count=1 --abbrev-commit)
export DOCKER_REGISTRY=collaborativeroom
export DOCKER_IMAGE_NAME=$(DOCKER_REGISTRY)/simple-micro-service:$(COMMIT_TAG)



build:
	go build -ldflags="-s -w -X main.version=$(COMMIT_TAG)" -o app internal/*.go

serve: build
	./app

clean:
	rm ./app

run:
	go run internal/*.go

pack:
	GOARCH=amd64 CGO_ENABLED=0 GOOS=linux make build
	docker build -t $(DOCKER_IMAGE_NAME) .

upload:
	docker push $(DOCKER_IMAGE_NAME)


deploy:
	envsubst < Deployment.yaml | kubectl apply -f -



destroy:
	envsubst < Deployment.yaml | kubectl delete -f -

ship: pack upload deploy clean
