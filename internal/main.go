package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
)

func main() {

	port := os.Getenv("PORT")



	name := os.Getenv("NAME")

	fmt.Println("Running on port " + port)

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(200)
		encoder := json.NewEncoder(w)
		if err := encoder.Encode(map[string]interface{}{
			"status":      200,
			"userMessage": "server deployed successfully, and updated 14 Mar 2019 14:03 "+ name,
		}); err != nil {
			w.WriteHeader(400)
		}
	})

	err := http.ListenAndServe(":"+port, nil)
	if err != nil {
		log.Fatal("Failed to start service on " + port)
	}

}
